//: [Previous](@previous)

import Foundation
import SwiftUI

// Sintaxis Enumeración

enum PersonalData {
    case name
    case surname
    case address
    case phone
}


var currentData: PersonalData = .name
var input = "Brais"

currentData = .phone
input = "66666"

// Enumeraciones con valores asociados

enum ComplexPersonalData {
    case name(String)
    case surname(String, String)
    case address(String, Int)
    case phone(Int)
}

var complexCurrentData: ComplexPersonalData = .name("Mayra")
complexCurrentData = .address("belgrano", 232)

// Enumeraciones con el mismo tipo de valor

enum RawPersonalData {
    case name
    case surname
    case address
    case phone
}
