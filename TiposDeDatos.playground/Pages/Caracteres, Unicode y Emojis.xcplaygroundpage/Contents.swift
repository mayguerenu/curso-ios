import Foundation

// Catacter
let myCharacter: Character = "\u{E9}"
let myCharacter2 = "\u{24}"

//Caracteres especiales (\n, \r, \t, \0, \')
let myString = "Hola \n cómo estás?"
let myString2 = "Bien \r Vos?"
let myString3 = "Todo \t bien"
let myString4 = "me \0 alegro"

//Emojis
let myEmoji = "\u{1f496}"
let myEmoji2 = "🕐"
