//: [Previous](@previous)

import Foundation

//var variable = "My playground"
//variable = "xcode, iOS, Swift, etc"
//
//print(variable)
//
//var variablebool =  true
//var variableint = 10
//
//print(variablebool)
//print(variableint)
//
//var variablefloat: Float = 10.0
//
//var coordinateX = 0.0, coordinateY = 0.0, coordinateZ = 0.0
//

let constante = "My playground"

typealias Name = String
let username: Name = "Mayra"

typealias AudioSample = UInt16
var maxAmplitudeFound = AudioSample.min


let user = ("Swift", "Beta")
user.0
user.1

let (firstValue, secondValue) = ("Swift", "Beta")
firstValue
secondValue



//: [Next](@next)
