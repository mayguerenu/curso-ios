import UIKit

// String en una línea
let myString = "Bienvenidos a mi hogar"

// String en varias líneas
let myMultipleString = """
Bienvenidos a mi hogar

Sientanse como en casa
"""

// String en varias líneas únicamente en código
let myFalseMultipleString = """
Bienvenidos a mi hogar\\

Sientanse como en casa
"""

// String vacío
let myEmptyString = ""
let myEmptyString2 = String ()

